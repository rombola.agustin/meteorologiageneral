# Notas de Meteorología General

## ATMÓSFERA

**Atmósfera**: es masa en movimiento y tiene distintas escalas espacio tiempo, por lo tanto no tiene una estructura hómogenea a lo largo de la componente vertical. Es la capa gaseosa que cubre la Tierra y que se mantiene atrapada a ella por la fuerza de gravitacional.

La mayor parte de la energía del Sol es abosorvida por superficie terrestre no por la misma atmósfera. La superficie terrestre calienta la capa de la atmósfera adyacente. El calentamiento desigual produce movimientos horizontales (viento) y verticales. 

La atmósfera y la superficie terrestre distribuyen la energía alrededor del globo y mantienen el balance de energía en el sistema Tierra-atmósfera devolviendo la misma cantidad de energía al espacio como la recibida desde el Sol. 

**Tiempo:** Es el estado de la atmósfera en un determinado instante y lugar.
**Clima:**Es con frecuencia definido como un "tiempo medio", es decir, un conjunto de condiciones normales que dominan una región, obtenidas de los promedios de las observaciones durante un cierto intervalo de tiempo.

Volviendo a la atmósfera, la temperatura en un planeta NO está, solamente, relacionado con la radiación solar incidente. La superficie de un planeta es también calentada mediante el efecto invernadero de su atmósfera.

**Efecto Invernadero:** Sin atmósfera la tierra tendría una temperatura media de -18ºC ya que la energía que la Tierra absorve sería reemitida en su totalidad. Sin embargo la atmósfera absorve parte de la energía gracias a la presencia de gases de efecto invernadero que provocan que sea reemitida a la Tierra de nuevo. Por lo tanto, gracias a la atmósfera, la Tierra tiene una temperatura media de 15ºC.


### SISTEMA CLIMÁTICO

**Atmósfera:** capa gaseosa que envuelve a la Tierra
**Hidrosfera:** el agua tanto dulce como salada en estado líquido tanto dulce como salada.
**Criosfera:** el agua en estado sólido
**Litosfera:** el suelo
**Biosfera:** los seres vivos que pueblan la Tierra 

### COMPOSICIÓN DE LA ATMÓSFERA

La atmósfera está compuesta en prinicipalmente por N2 y O2. 

**Ley de Boile:** P . V = cte  *(A temperatura constante)*
**Ley de Charles:** V = cte * T  *(A presión constante)*
**Ley de Estado de los gases ideales:** P . V = n . R . T

Se puede reemplazar ro = m/v y de esa forma se obtiene: P = ro . R . T

![alt text](composicion.png)

**NITRÓGENO:** Es *removido* de la atmósfera principalmente por procesos biológicos que involucran a las bacterias presentes en los suelos. *Retorna* a la atmósfera principalmente a través de la degradación de la materia orgánica por la acción de microorganismos. 

**OXÍGENO:** Es *removido* de la atmósfera con la degradación de la materia orgánica y con los procesos de oxidación en los que el oxígeno se combina con otras sustancias. El oxígeno también es *consumido* en la respiración de los seres vivos, en la cual se libera además dióxido de carbono. La *adición* de oxígeno a la atmósfera ocurre en los procesos de fotosíntesis. 

**VAPOR DE AGUA:** La concentración del vapor de agua, varía enormemente de un lugar a otro. Cerca de la superficie, en las regiones tropicales, el vapor de agua puede constituir hasta el 4% de los gases atmosféricos, mientras que en regiones polares representa bastante menos del 1%. *Además el vapor de agua constituye una reserva de calor muy importante* Durante la condensación del vapor de agua se libera una gran cantidad de energía que constituye el motor de fenómenos meteorológicos.

**DIÓXIDO DE CARBONO (CO2):** Entra principalmente a la atmósfera por la degradación de la materia vegetal, pero también lo hace por las erupciones volcánicas, la respiración de los seres vivos y por actividades humanas como el uso de combustibles y la deforestación. Es removido de la atmósfera por los procesos de fotosíntesis. 

**METANO (CH4):** El metano se produce en forma natural por la descomposición de sustancias orgánicas en ambientes pobres en oxígeno. También se produce en el sistema digestivo de rumiantes y otros animales, en la explotación de combustibles fósiles y en la quema de biomasa.

**OXIDOS NITRICO/OSO:** Están asociados al fenómeno de lluvia ácida y colaboran con la desintegración del ozono.

### Aire

El aire que está atmósfera está dividido en 3 categorías: *Vapor de Agua, Aerosoles y aire seco* 

**Aire húmedo = Aire seco + Vapor de Agua**

**IMPORTANTE --> EL N2 Y EL O2 NO CONTRIBUYEN EN LOS PROCESOS METEOROLOGICOS. LOS QUE JUEGAN UN ROL FUNDAMENTAL SON EL VAPOR DE AGUA, EL CO2, EL O3 Y LOS AEROSOLES**


### Estructura vertical de la Atmósfera

![alt text](structure.jpg)

**ESTRATÓSFERA** 
Se extiende hasta los 45 km de altura. En ella la temperatura aumenta con la altura hasta un valor cercano a 0°C en su límite superior denominado estratopausa. En la estratósfera se concentra la máxima concentración de ozono (“capa de ozono”).

**MESÓSFERA**
La temperatura disminuye con la altura y culmina a unos 80 km de altitud donde la temperatura es del orden de -90°C (mesopausa). Por encima de ese nivel, y hasta un nivel superior no bien definido la temperatura vuelve a aumentar con la altura definiendo la capa denominada TERMÓSFERA.

**IONÓSFERA** 
Es una parte especial de la atmósfera que forma parte de la termósfera. Representa menos del 0.1% de la masa atmosférica.A medida que se asciende en la ionosfera, la temperatura aumenta. La comunicación a larga distancia por radio es posible ya que las diferentes regiones de la ionósfera reflejan las ondas radiales de regreso a la Tierra. Aquí es donde suceden las auroras.

## RADIACIÓN

La *temperatura* es una magnitud física que da una medida de cuán frío o caliente está un objeto en relación con un valor standard. 

El *calor* es transferencia de energía. Hay 3 tipos de calor: *convección, condución y radición*.

**Convección** Es la transferencia de calor a través del movimiento de masa en un fluido. Se produce en líquidos y gases ya que en ellos los átomos y moléculas pueden moverse libremente.
La convección ocurre naturalmente en la atmósfera dando lugar a una circulación convectiva. 

*AIRE DESCIENDE -> SE CALIENTA -> SE COMPRIME*
*AIRE ASCIENDE -> SE ENFRÍA -> SE EXPANDE*

![alt text](termicas1.png)

![alt text](termicas2.png)

![alt text](termicas3.png)

El 40% de la energía emitida por el sol es del tipo *Infraroja* y el 10% es de tipo *ultravioleta*. Por lo que el 44% es parte de *espectro visible*

**CUERPO NEGRO** Es un cuerpo que emite (o absorbe) radiación electromagnética con un 100% de eficiencia en todas las longitudes de onda. La superficie terrestre y el Sol absorben y emiten radiación con una eficiencia cercana al 100% y pueden ser considerados cuerpos negros.

**Leyes de radiación:**

1) Todos los objetos emiten energía radiante, cualquiera sea su temperatura
2) Los objetos con mayor temperatura emiten más energía total por unidad de área que los objetos más fríos
3) Los cuerpos con mayor temperatura emiten un máximo de radiación en longitudes de onda más cortas que los que se encuentran más fríos
4) Los objetos que son buenos absorbedores de radiación son también buenos emisores (ley de Kirchoff)

**Radición que llega a la atmósfera**

![alt text](radiacion.png)

**Dispersión**

Depende de la relación entre el tamaño de las partículas y la longitud de onda.

*Dispersión de Rayleigh* --> Cuando lambda > d donde d es el diámetro de la partícula.
Depende de la longitud de onda y su efecto es inversamente proporcional a la potencia cuarta de la longitud de onda. La dispersión de la luz azul es aproximadamente 16 veces mayor que la de la luz roja para igual intensidad incidente. 

**Dispersión de Mie** --> Cuando lambda = d aprox. 

**Absorción** El nitrógeno es un mal absorbedor de la radiación solar. El oxígeno y el ozono absorben eficientemente radiación ultravioleta en longitudes de onda menores a 290 nm. El *vapor de agua* absorbe longitudes de onda largas.

## SISTEMA SOL-TIERRA

**Rotación** La tierra rota sobre si misma con un período de 24 hs.

**Translación** La Tierra describe una órbita elíptica alrededor del Sol. Su período es de 365 días y cuarto. La tierra tiene su eje inclinado unos 23,5º

**Zenith:** ángulo del sol sobre la vertical.

**CONSTANTE SOLAR** --> Valor medio = 1368 W/m2 *(Tiene variaciones del orden del 0.1% a lo largo del ciclo solar de 11 años)*

El vapor de agua y el dióxido de carbono son importantes absorbedores de radiación infrarroja contribuyendo de esta forma a elevar la temperatura de los niveles más bajos de la atmósfera (tropósfera baja). *La atmósfera no absorbe radiación de onda larga enla banda entre 8 um y 11 um.*

### **La atmósfera no es calentada por la radiación solar sino que se calienta desde la superficie de la Tierra hacia arriba.**

**Calor sensible** Cantidad de calor que cede o absorbe un cuerpo sin cambiar de estado. Puede ser detectado a través del cambio de temperatura del cuerpo.

**Calor latente** Cantidad de calor que cede o absorbe un cuerpo al cambiar de estado. Durante el cambio de estado no se modifica la temperatura del cuerpo.

![alt text](radiationbalance.png)

![alt text](netbalance.png)

## OZONO ESTRATOSFÉRICO

El ozono es O3. Aproximadamente el 90% está en la baja estratósfera. El 10% restante está en la tropósfera.

![alt text](ozonocolumn.png)

### Mecanismo de Chapman

Para la formación de O3 es necesaria la presencia de oxígeno atómico (O) y molecular (O2) y radiación solar.

![alt text](o3formation.png)

El oxígeno atómico (O) es **máximo** cerca de los 95 kilómetros de altura y disminuye hacia la superficie terrestre mientras que el oxígeno molecular presenta mayor densidad cerca de la superficie del planeta. En consecuencia, **existe una altura óptima para la formación de ozono que se encuentra alrededor del nivel medio de la estratosfera (20 a 30 km de altura)**

### **O2 + UV (longitud de onda <240 nm) --> O + O** 
### ** O + O2 + M --> O3 + M**

*Se necesita además de la presencia de moléculas de otros elementos químicos (por ejemplo oxígeno o nitrógeno moleculares, representados por la letra “M”) que absorban parte de la energía*

### Destrucción del ozono en la atmósfera

La destrucción natural del ozono ocurre también a través de una serie de procesos en los que intervienen la radiación solar y el oxígeno.

### **O3 + UV (longitud de onda < 310 nm) --> O2 + O**
### **O + O3 --> O2 + O2**

**IMPORTANTE** --> El mecanismo de Chapman daría como resultado 30% más ozono del que realmente existe. Por lo que tienen que existir otras reacciones químicas que jueguen un rol en este proceso.

![alt text](o3absorcion.png)

La ozogénesis (creación del O3) y ozonólisis(destrucción del O3) son reacciones que conducen a un equilibrio fotoquímico mediante el cual se mantiene una pequeña concentración de ozono en la alta atmósfera que protege a manera de escudo a los seres vivos que habitan la Tierra de los rayos ultravioletas.

**El óxido nítrico (NO), que puede producirse a partir del óxido nitroso (N2O), puede facilitar la destrucción de ozono a partir de las siguientes reacciones**:
### **NO + O3 --> NO2 + O2**
### **NO2 + O --> NO + O2**
### **Balance neto: O3 + O --> O2 + O2**

Este proceso de destrucción es un ejemplo de un **ciclo catalítico**, en donde un conjunto de reacciones químicas son facilitadas por la presencia de un catalizador (sustancia que facilita una reacción química pero que no se altera a causa de la reacción).

### Capa de Ozono

Las observaciones realizadas sobre la concentración del ozono estratosférico desde comienzos de la década del '80, permitieron constatar que durante los meses de septiembre y octubre de cada año se produce en la región antártica una vertiginosa caída en la concentración del ozono a niveles inferiores de 220 UD.

![alt text](ozonehole.png)

El “agujero de ozono” de 2006 es considerado como el más severo debido a que alcanzo el mínimo de ozono según el satélite Aura de la NASA: 82 UD el 9/10 sobre una región de la Antártida.

### Causas del agujero de ozono
**a) la circulación atmosférica**

La circulación del aire en la estratósfera sobre los polos está gobernada por la diferencia en la insolación entre el verano y el invierno. El vórtice polar puede ser imaginado como una pared de un recipiente de 500 km de diámetro que aísla casi por completo el aire de su interior e impide el intercambio gaseoso del aire que se encuentra dentro con masas de aire procedentes de latitudes más bajas,

![alt text](polarvortex.png)

El cloro se almacena como cloro molecular (Cl2) inerte en la oscuridad pero con una vida media de una hora después de la llegada de la luz solar. Al término de la noche polar, el Cl2 hasta entonces inactivo, se separa en dos átomos de cloro (Cl) muy reactivos antes la presencia de la radiación UV.

### Cl3CF + UV (longitud de onda < 230 nm) --> Cl2CF + Cl
### Cl + O3 --> ClO + O2
### ClO + O --> Cl + O2
### Balance neto: O3 + O --> O2 + O2

De esta manera, un solo átomo de Cl es capaz de dar origen a una reacción que destruye 100.000 moléculas de ozono.

**b) los clorofluorocarbonos (CFCs) y otras sustancias**

Las concentraciones de cloro de origen natural son muy bajas, especialmente en la estratosfera y por consiguiente no pueden explicar los niveles de destrucción de ozono observados. En términos químicos, los CFCs son derivados de hidrocarburos. Son relativamente insolubles en agua y por ello no son removidos de la atmósfera por la lluvia, ni se disuelven en los océanos. Tienen una supervivencia en la atmósfera de entre 50 y 100 años y ante la radiación UV se disocian y dan comienzo al proceso de destrucción del ozono indicado.

**c) las nubes estratosféricas polares (NEP)**

Dentro del vórtice polar, la temperatura desciende hasta niveles entre -80ºC y -90ºC dando origen a la formación de las nubes estratosféricas polares (NEP) a aproximadamente 20 km de altura. Estas nubes, de forma lenticular y aspecto nacarado, a diferencia de las que se forman en la troposfera, están compuestas por partículas de cristales de hielo puro y ácido nítrico.

### Diferencias entre HN y HS

Distribución desigual de tierras y mares sobre los dos hemisferios condiciona la evolución del vórtice polar.

## TEMPERATURA

Veamos los factores que controlan la temperatura en una determinada región geográfica:

### 1) Componente radiativa

Estos factores son los que determinan el balance local de radiación y en consecuencia la temperatura local del aire: latitud, hora del día y día del año: determinan la altura del sol y la intensidad y duración de la radiación solar incidente.

- Mayor en los trópicos y menor en latitudes medias,
- Mayor en enero que en julio (en el Hemisferio Sur),
- Mayor durante el día que en la noche,
- Mayor con cielo claro que nublado (durante el día),
- Mayor con suelo descubierto que con nieve y cuando está seco en lugar de húmedo

![alt text](marchaanual.png)

*En general, la temperatura mínima ocurre cerca del amanecer como resultado del enfriamiento radiativo nocturno de la superficie terrestre. La temperatura máxima ocurre usualmente en la tarde, algunas horas después del mediodía* El defasaje entre temperatura y radiación a lo largo del día es
consecuencia principalmente del proceso de calentamiento de la atmósfera

Factores que influyen en la amplitud térmica diurna:

- Variación de la altura del sol durante el día
- Nubosidad
- Influencia del mar

![alt text](marchadiaria.png) 

### 2) Advección de masas de aire

Advección es el movimiento de una masa de aire desde un lugar hacia otro.
*Fría:* cuando el viento sopla a través de isotermas de un área mas fría hacia otra más caliente
*Caliente:* cuando el viento sopla a través de isotermas de un área mas caliente hacia otra más fría

**Isotermas:** son líneas trazadas sobre un mapa que unen puntos con la misma temperatura del aire.

### 3) Calentamiento diferencial tierra-agua

El suelo se calienta más rápido y alcanza mayores temperaturas que el agua y también se enfría más rápidamente y a temperaturas más bajas que el agua.

- El agua es altamente móvil: cuando se calienta, la turbulencia distribuye el calor a través de una masa mucho mayor
- El suelo es opaco: el calor es absorbido únicamente en la superficie. El agua al ser transparente permite que la radiación solar penetre a profundidades de varios metros.
- El calor especifico es casi 3 veces mayor para el agua que para el suelo.
- La evaporación (proceso de enfriamiento): es mayor sobre el agua que sobre la superficie terrestre.

### 4) Corrientes oceánicas

Los efectos de las corrientes oceánicas sobre la temperatura de áreas adyacentes es variable. Las corrientes cálidas, cuando se dirigen hacia los polos tienen un efecto moderador del frío. Por ejemplo, el efecto de la corriente cálida del Atlántico Norte sobre Europa.

### 5) Altura

La disminución vertical de la temperatura en la troposfera es en promedio de de 6.5°C/km. Sin embargo cuando se mide la temperatura en lugares elevados y se los compara con otros en la misma latitud pero a menor altura se encuentran diferencias con esta tasa de enfriamiento debido a la absorción y re-radiación de la energía por parte del suelo.

### 6) Componente geográfica

- **Efecto de brisa mar-tierra** Brisa diaria que sopla desde la tierra hacia el mar. Su causa radica en la diferencia de temperatura entre la superficie del mar que es más cálida que la de las tierras adyacentes. Predominan durante la noche y culmina al amanecer. Brisa costera diaria que sopla desde el mar hacia la tierra. Su causa radica en la diferencia de temperatura entre la superficie de la tierra que es más cálida que la del adyacente cuerpo de agua. Predomina durante el día y culmina a media tarde. 
- **Efecto isla de calor** Es el calentamiento diferencial que se registra en áreas urbanas en comparación con las zonas rurales próximas a la ciudad.
- **Efecto de barreras orográficas** En general la temperatura es menor en barlovento que en sotavento (barlovento es el lado de una montaña donde sopla el viento de forma ascendente)
- **Efecto monzónico** Las intensas precipitaciones que se registran durante el verano producen un mínimo de temperatura durante el otoño cuando debería registrarse un máximo según la componente radiativa.


## HUMEDAD EN LA ATMÓSFERA

### Ciclo hidrológico

La cantidad total de agua en el planeta no cambia. Aproximadamente la proporción es:

- Hielo: 61,84%
- Humedad en el suelo: 0,30%
- Agua subterránea: 37,40%
- Rios y lagos: 0,44%
- Atmósfera: 0,03%


![alt text](ciclohidrologico.png) 

Corrientes ascendentes de aire llevan el vapor a las capas superiores de la atmósfera, donde la menor temperatura causa que el vapor de agua condense y forme nubes. Al evaporarse, el agua deja atrás todos los elementos que la contaminan o la hacen no apta para beber (sales minerales, químicos, desechos). *Por eso el ciclo entrega agua en estado puro.*

**Aproximadamente el 80 % del agua la evaporada total proviene de los océanos, mientras que el 20 % restante lo hace del agua de las regiones continentales y de la transpiración de la vegetación. La mayor parte del agua no se transporta en forma líquida sino en forma de vapor de agua** 

Además de la evaporación desde el suelo, ríos y lagos, una parte del agua que se infiltra es absorbida por plantas que la liberan a la atmósfera a través de la transpiración. Las medidas de evaporación directa y transpiración son usualmente combinadas como evapotranspiración.

**La transpiración representa aproximadamente el 10 % de toda el agua evaporada que ingresa a la atmósfera.**

### Variables de Humedad

**1) Presión parcial de vapor o Tensión de vapor (e)**
La presión asociada con cada uno de los gases de la mezcla se denomina presión parcial. En el caso del vapor de agua se denomina tensión de vapor.

### **Presión atmosférica = Presión parcial + e del aire seco**

**Saturación:** El aire puede contener una cierta proporción de vapor de agua ya que a partir de cierto umbral el vapor comienza a condensar en agua líquida que es luego re-evaporada. Al aire que contiene ese valor umbral de vapor de agua se dice que está **saturado**. El valor de la **tensión de vapor** a la que ocurre este fenómeno se denomina tensión de vapor de saturación (e_s).

Hay dos mecanismos por los cuales puede alcanzarse la saturación:
- *Humidificación* (aumentar la cantidad de vapor por medio de evaporación, T=cte)
- *Enfriamiento isobárico* (p=cte)

La *tensión de vapor* depende de la temperatura: el aire cálido puede albergar más vapor de agua que el aire relativamente más frío.

![alt text](saturacion.png) 

**2) Humedad absoluta**

### HA = masa de vapor de agua / volumen 

**3) Humedad específica**

### q = masa de vapor de agua / masa total de aire

![alt text](humedadespecifica.png)

**4) Relación de Mezcla** 

### w = masa de vapor / masa de aire seco

**5) Humedad relativa**

### HR = ( e / e_s ) * 100
donde e = tensión de vapor , e_s = tensión de vapor de saturación

--> La humedad relativa puede sufrir cambios por cambios en al cantidad de vapor en el aire o por cambios de temperatura que producen cambios en la tensión de vapor de saturación.

**6) Punto de Rocío (Td)**

Es la temperatura hasta la cual habría que enfriar el aire para alcanzar la saturación manteniendo la presión constante (proceso isobárico) y sin agregar vapor de agua.

### Parcelas de aire. Proceso adiabático. Estabilidad atmosférica.

**Parcelas de aire:** Usaremos el nombre de parcela de aire para identificar un gran número (millones) de moléculas que se mueven en forma más o menos coherente empleando un volumen de varios m3: “un globo grande pero sin sus paredes”.

**Proceso adiabático**

Una parcela cuando *asciende* se *expande* debido a que la presión externa disminuye. Las moléculas usan parte de su *energía interna* en el proceso de expansión (presionan hacia afuera) y por lo tanto se *enfría*. 

*En este proceso, no se ha sacado ni inyectado calor externo a la parcela, por lo cual se denomina proceso adiabático.*

![alt text](asciendeyenfria.png)

Las moléculas que están dentro de la parcela deben utilizar parte de su energía interna para realizar este trabajo y por ese pierden energía y se enfrían.

Todo este proceso se llama **enfríamiento adiabático** y el proceso contrario por el cual una parcela desciende y se calienta se llama **calentamiento adiabático**.

**Gradiente adiabático (seco):** “Seco”= indica que la parcela de aire no está saturada, en un ascenso o descenso adiabático el gradiente
(cambio) de temperatura con la altura es gamma = +- 10ºC/Km. O sea por cada km de ascenso la temperatura desciende 10ºC y visiversa si desciende. *Si el ascenso/descenso toma menos de un día la aproximación es relativamente buena*.

Se puede calcular el valor del gradiente adiabático seco a partir de la Primera Ley de la Termodinámica que establece la conservación de la energía:

### Delta U = Q - W 

La energía ganada/perdida por una parcela de aire seco es igual a la suma del cambio en su energía interna más el trabajo realizado durante la expansión.

Otra forma alternativa es: 

## dq = Cp . dT - a . dP

- dq = energía ganada/perdida
- Cp = calor específico a presión constante
- dT = delta de temp.
- a = volumen específico (la inversa de la densidad)
- dP = Trabajo realizado

**Nivel de condensación por ascenso (NCA)**
Es la altura a la cual una parcela de aire se saturará cuando es elevada adiabáticamente seca. Señala el nivel a partir del cual es esperable la formación de nubosidad.

**Gradiente adiabático saturado:** gamma = -6ºC/Km Esto es, por cada kilómetro de ascenso (descenso) la temperaturade la parcela disminuye (aumenta) en 6°C.

![alt text](mecanismoascenso.png)

Una vez que se dió el ascenso luego pueden suceder 3 cosas:

- **Estable** Parcela tiende a volver a su nivel inicial
- **Inestable** Parcela continua subiendo (sin necesidad de forzamiento)
- **Neutro** Parcela se mantiene en el nivel que quedó

La estabilidad del aire se determina comparando la temperatura de la parcela de aire que asciende con la del aire circundante (entorno)

![alt text](estable.png)
![alt text](inestable.png)

## NUBES Y NEBLINAS

**Nubes** Son la manifestación visible de la condensación y deposición del vapor en la atmósfera.

![alt text](formaciondenubes.png)

En la atmósfera existen *NÚCLEOS* (pequeñas partículas sólidas o líquidas que proveen grandes áreas sobre las que la deposición puede realizarse). Pueden ser naturales o antropogénico.

- Núcleos de condensación
- Núcleos de formación de hielo de congelación o de deposición

![alt text](tipodenubes.png)

### Niebla y neblina

Son, al igual que las nubes, la manifestación visible de la condensación y deposición del vapor en la atmósfera. La diferencia es que se producen próximas a la superficie y que los mecanismos de formación también son diferentes.

- Visibilidad < 1 km NIEBLA
- Visibilidad entre 1 y 5 km NEBLINA

Las nieblas se pueden clasificar por espesor vertical, densidad, persistencia y momento de ocurrencia. Hay 3 grandes tipos:

- 1) Nieblas por evaporación

Se producen cuando se evapora agua en el aire frío. El aumento del contenido de vapor en el aire se puede producir cuando una corriente de aire frío y seco fluye sobre una superficie de agua de mayor temperatura. El vapor se eleva y al mezclarse con el aire frío, condensa. Al evaporarse el agua, enriquece en vapor el aire húmedo que tiene encima, provocando un aumento de la HR, llegando a la saturación y a la formación de la niebla

- Nieblas por radiación

Se generan por la disminución que experimenta la capacidad del aire para retener vapor de agua cuando disminuye la temperatura. Se producen cuando hay un proceso de enfriamiento *isobárico* donde la presión se mantiene constante pero el suelo se enfría al llegar la noche por lo que el aire que lo rodea también lo hace. Eso puede producir que la temperatura descienda hasta alcanzar la de rocío y entonces al llegar a una HR de 100% se producen nieblas.
Para ello es necesario que el cielo esté casi claro o claro y que las velocidades del viento sea muy baja (entre 3 y 13 km/h) con una humedad relativa alta. 

- Nieblas por advección

Se generan cuando una corriente de aire cálido y húmedo se desplaza sobre una superficie más fría. El aire se enfría desde abajo, su humedad relativa aumenta y el vapor de agua se condensa formando la niebla. Para que este tipo de niebla se forme es necesario que el viento sople con una intensidad entre 8 y 24 km/h para que se pueda mantener constante el flujo de aire cálido y húmedo. Si el aire está calmo, el vapor de agua se depositará sobre el suelo formando rocío. 

## PRECIPITACIÓN

Es la humedad condensada que cae hasta la superficie terrestre --> La condensación es un requisito previo: Núcleos de condensación
![alt text](gotas.png)

Una gota de agua está sometida a la aceleración de gravedad hacia abajo y a la fuerza de fricción, producida por el aire que la rodea que se incrementa a medida que la velocidad de la gota aumenta mientras cae. Llega un punto que la fuerza de fricción y la fuerza de gravedad se compensan por lo tanto a = 0 y entonces se alcanza una *velocidad terminal*. Las gotas pueden llegar hasta 5 mm y en ese caso tienen una velocidad terminal de 9 m/s. 

**NUBE FRÍA:** T<0ºC en toda la nube
**NUBE CALIENTE:** T>0ºC en la base y T<0ºC en el tope

### Colisión-Coalescencia: Creciemiento en nubes cálidas

Nubes con temperaturas >0ºC predominan en las regiones tropicales y de latitudes medias durante la estación cálida (nubes cálidas). El proceso se inicia con grandes gotas colectoras que poseen velocidades terminales altas. 

- Si las gotas son demasiado pequeñas: la compresión del aire por debajo de la gota que cae, fuerza el movimiento de las gotas pequeñas hacia afuera
- Si las gotas son demasiado grandes (del tamaño de la gota colectora): caerá a la misma velocidad y no habrá colisión

--> Cuando ocurre la colisión, las gotas o bien se separan o *coalescen* en una gota más grande.
--> La eficiencia de la coalescencia es en gral. muy alta indicando que la mayoría de las colisiones termina en dos gotas uniéndose

### Proceso de Bergeron: crecimiento en nubes frías

Son comunes en latitudes medias. Las nubes están compuestas en general por: Agua líquida, Agua sobreenfriada y/o Hielo. (El agua sobreenfriada puede existir con temperaturas de hasta -40ºC ya que para que se forme hielo se requiere de núcleos de congelación que a diferencia de los núcleos de condensación son poco abundantes)
Cuando hay en forma simultánea cristales de hielo, gotas de agua y vapor de agua, comienza este proceso.
La tensión de vapor de saturación con respecto al hielo es menor que con respecto al agua a la misma temperatura. Los cristales de hielo crecen rápidamente a expensas de las gotas sobreenfriadas

### Acreción y Agregación

En gral. el proceso de Bergeron no es capaz de producir cristales lo suficientemente grandes para que haya precipitación.
- Acreción = el agua liquida se congela sobre los cristales de hielo 
- Agregación = se unen cristales de hielo 

La colisión combinada con la acreción y agregación permite la formación de cristales lo suficientemente grandes como para precipitar dentro de la 1/2 hora inicial de su formación.

![alt text](acrecion.png)

Inicialmente son esféricas pero a medida que actúa la fuerza de fricción se van deformando. El tamaño maximo de una gota es de aprox. 5 mm de
diámetro.

**Tipos de Lluvia**

- Lluvia: es la precipitación de agua líquida que llega al suelo con gotas de diámetro entre 0,5 y 6 milímetros. Si la lluvia no llega al suelo porque se evapora durante su descenso, se forma una especie de cortina que cuelga de la base de la nube llamada virga.
- Llovizna: está formada por gotas pequeñas de menos de 0.5 mm de diámetro.
- Nieve: se forma cuando el vapor de agua se congela en cristales diminutos de hielo en niveles de la atmósfera donde las temperaturas son muy inferiores a 0ºC. Los cristales de hielo se van uniendo y forman los copos de nieve. Cuando estos copos tienen suficiente peso, caen al suelo.
- Aguanieve: mezcla de nieve y lluvia
- Granizo: precipitación congelada en hielo duro de
forma redondeada o irregular

![alt text](hail.png)
![alt text](precipitacion.png)

## PRESIÓN

La **presión atmosférica** en un cierto punto corresponde a la fuerza (peso) que la columna atmosférica sobre ese lugar ejerce por unidad de área, debido a la atracción gravitacional de la Tierra. 

Variación vertical de la presión --> ecuación hidrostática: *- dp = g.rho.dz*

Si se quieren realizar comparaciones entre las mediciones de presión de distintos lugares, es necesario hacer una corrección por altura y reducirlos al nivel del mar.

Por las diferencias en la densidad del aire como consecuencias de las diferencias de temperatura o contenido de vapor de agua o ambas se producden diferencia de presión horizontales.

Si la temperatura del aire sube, sus moléculas tienen mayor movimiento. Si el aire fue calentado en un recipiente cerrado, su presión sobre las paredes internas aumentará, a medida que las moléculas con más energía bombardean las paredes con más fuerza y la densidad del aire no se alterará. 
**Cuando el aire es calentado, el espaciamiento entre moléculas aumenta y la densidad disminuye, provocando una disminución de la presión: para volúmenes iguales el aire caliente es menos denso que el aire frío. **
**La mayor presencia de vapor de agua en el aire disminuye la densidad del aire porque el peso molecular del agua (18,016 kg/mol) es menor que el peso molecular medio del aire (28,97 kg/mol). Por lo tanto, a iguales temperaturas y volúmenes, una masa de aire más húmeda ejerce menos presión que una masa de aire más seca.**

Húmedo y caliente --> Menor presión
Seco y frío --> Mayor presión

### Isobaras
Las *isobaras* son líneas que unen puntos de igual presión. Por ejemplo, dan idea de la intensidad del viento (a mayor proximidad entre isobaras, mayor intensidad), así como de su procedencia.

*gradiente presión = delta P / delta n* donde delta n es la distancia entre isobaras

### Ciclones y Anticiclones

Cuando en un mapa de isobaras existe una zona en la que la presión es más alta que a su alrededor y las isobaras son cerradas aparece una “A” y decimos que hay un *anticiclón*. Si por el contrario la presión empieza a decrecer, en el punto en el que alcanzan su valor mínimo aparece una “B” y decimos que hay una zona de baja presión, *depresión o ciclón* cuando las isobaras aparecen cerradas a su alrededor.

![alt text](ciclon.png)

Donde las isobaras son curvas sin cerrarse, a las regiones de altas presiones se les llama **cuñas** y a las de bajas presiones **vaguadas**. 

![alt text](ciclon2.png)

## FUERZAS EN LA ATMÓSFERA

### 1)  Fuerza de gravedad

P = m.g

### 2) Fuerza de presión (Fp)

La fuerza de presión es la forma que tiene la atmósfera de tratar de balancear las diferencias en el campo de presión. Fluye de donde hay alta presión a baja presión. **La fuerza de presión tiene la misma dirección que el gradiente de presión y sentido opuesto**

- Dirección de Fp: Siempre de ALTAS a BAJAS presiones y perpendicular a las isobaras
- Magnitud de Fp: Relacionada con la distancia entre isobaras. Cuando las isobaras están muy apretadas la fuerza es mayor que cuando están más espaciadas.

![alt text](mapapresiones.png)

### 3) Fuerza de Coriolis (Fco)

La fuerza de Coriolis es una fuerza aparente y existe sólo para un observador en un sistema de referencia en rotación. 

**F = 2 . omega . v . sen phi** donde phi es la latitud !

Dirección de FCo: actúa hacia la izquierda del flujo en el hemisferio sur y hacia la derecha en el hemisferio norte. Es siempre perpendicular al movimiento. 

Si el viento va hacia el ecuador --> Se tuerce hacia la izquierda
Si el viento va hacia los polos --> Se tuerce ahacia la derecha

### 4) Fuerza de fricción

Es una fuerza causada por el flujo de aire sobre una rugosidad de la superficie terrestre. Se opone o desacelera el viento. Es más importante cerca de la superficie terrestre (altura < 1 km)

**Fr = - c . V**

## VIENTO

Las parcelas de aire se mueven en la horizontal y vertical, con velocidad variable. El viento se asocia con la componente horizontal. Los movimientos verticales son, generalmente, mucho más débiles que los horizontales.

La dirección del viento se designa según la dirección geográfica desde donde el viento esta soplando (desde donde viene).

**La principal fuerza que produce el viento es la fuerza de gradiente de presión.** Los gradientes de presión en la atmósfera son muchas veces generados por calentamiento/enfriamiento diferencial del aire.

![alt text](ejemplopresion.png)

![alt text](vientosHS.png)

### Viento Geostrófico

Para la mayoría de los sistemas del tiempo, la fuerza de gradiente de presión tiende a estar en balance con la fuerza de
Coriolis. El viento que resulta de este balance se denomina viento geostrófico y es una excelente aproximación del viento real en los niveles de la atmósfera donde la fricción es despreciable (sobre el océano o por encima de los 1000m de altura). El viento gesotrófico es paralelo a las isobaras y su magnitud es proporcional al gradiente de presión. Su sentido depende del hemisferio.

### El viento geostrófico (Vg) deja las bajas presiones a la derecha en el HS y a la izquierda en el HN. 

El viento geostrófico es el viento que habría en la atmósfera si el movimiento fuese:
- horizontal
- sin aceleración
- sin fricción

**Bajo estas suposiciones, la aproximación de balance geostrófico (Fp =FCo) no es válida en latitudes ecuatoriales.**

![alt text](bajasyaltas.png)

## Circulación general de la atmósfera

Se llama circulación general de la atmósfera al sistema de vientos en escala planetaria. Las latitudes altas reciben radiación con menores
ángulos incidentes y en el ecuador practicamente con 90º. Por lo tanto el flujo de energía por unidad de área es mayor cerca del ecuador. Aunque en ecuador permanentemente reciban mayor energía solar por m2, su temperatura NO está aumentando en forma permanente gracias al equilibrio térmico del planeta. La **circulación de la atmósfera y los océanos** distribuye el exceso de energía que reciben las zonas tropicales hacia latitudes altas, manteniendo así el equilibrio térmico del planeta.

### Modelo de celda térmica

Es el modelo primitivo más elemental de circulación global. Sugiere la existencia de una sola celda térmica directa de circulación vertical llamada **Celda de Hadley**. 

![alt text](celdatermica.png)

Las suposiciones de que la atmósfera se comporta como una única celda de Hadley son las siguientes:

- La superficie terrestre está cubierta uniformemente por agua
- Los rayos del Sol están siempre dirigidos hacia el ecuador
- La Tierra no rota (solo existen fuerzas de presión pero no coriolis)

![alt text](celdaunica.png)

Ahora... si se agrega que la tierra rota empieza a actuar coriolis. 

![alt text](celdacoriolis.png)

Resulta que en verdad existe una celda que esta limitada desde el ecuador hasta la latitud 30º. En el ecuador entonces existe una zona donde convergen los vientos de ambas celdas. Esa región no es estática y se la llama **Zona de convergencia Intertropical (ZCIT)** Como en dicha zona hay convergencia que produce ascenso de aire, se forman sistemas de bajas presiones. 

### Modelo de 3 celdas

Dicho modelo es más cercano a la realidad. La atmósfera tiene 3 celdas de circulación por hemisferio. 

**1) Celda de Hadley (de 0º a 30º)**

En el ecuador el aire más cálido que se eleva, se condensa liberando calor latente y formando grandes cúmulos y cumulonimbus que producen abundante precipitación, que mantienen la densa vegetación de las selvas tropicales. En la latitud 30º, el flujo se separa en una rama hacia el ecuador y otra hacia los polos. El flujo de superficie hacia el ecuador es desviado por la fuerza de Coriolis, generándose los **vientos alisios** que tienen la característica de ser de intensidad moderada y muy persistentes en dirección. Los vientos alisios soplan *del sudeste en el HS* y *del noreste en el HN*, convergiendo en el ecuador en una región con un gradiente de presión muy débil, llamada zona de calmas ecuatoriales (o doldrums).

**2) Celda de Ferrel (de 30º a 60º)**

Desde la celda de Hadley, por la divergencia en latitudes medias, la rama del flujo en superficie que se separa hacia los polos, es desviado por el efecto Coriolis, produciéndose una fuerte componente hacia el oeste, generando un sistema de vientos conocidos como los **vientos del oeste.** La franja latitudinal de convergencia de ambos sistemas de vientos se llama la región del frente polar. Es la región más dinámica de la atmósfera, donde se desplazan de oeste a este, en promedio, los centros ciclónicos que se asocian a los sistemas frontales de latitudes medias, generando un tiempo con vientos muy intensos y variables, con abundante nubosidad y precipitación.

La celda de Ferrel es **indirecta** porque el aire cálido es forzado a descender en latitudes subtropicales entorno a los 30º y a moverse en superficie desde latitudes subtropicales mas cálidas hacia zonas subpolares mas frías, donde el aire frío es forzado a elevarse. 

**3) Celda Polar (de 60º a 90º)**

En los polos, producto de la presencia de aire muy frío, se producen altas presiones en superficie y divergencia. Por lo tanto en dicha zona el aire desciende. Caso contrario en las latitudes 60º se producen sistemas de bajas presiones y convergencias en superficie por lo que aire asciende enfríandose. El aire es desviado fuertemente por la fuerza de coriolis que como depende del seno de la latitud es muy fuerte en dicha región. Dichos vientos se conocen como **estes polares** Alrededor de los 60º de latitud se produce convección y flujo hacia los polos en altura, cerrándose una celda de circulación directa llamada celda Polar.

![alt text](esquemacirculacion.png)

- **Entre los trópicos se tiene una zona de bajas presiones ecuatoriales, donde convergen los vientos alisios del SE y del NE produciendo movimientos ascendentes, con convección profunda y abundante nubosidad con precipitación continua e intensa. Esta región de encuentro de los alisios se conoce como la zona de convergencia intertropical (ZCIT).** 

- **Entre 25 y 35º de latitud, donde se originan los vientos alisios, se tiene la zona de altas presiones subtropicales. En esta franja se produce subsidencia y divergencia en superficie, los gradientes de presión son muy débiles por lo que los vientos son flojos y variables.**

- **Entre 45 y 60º de latitud se encuentra una franja de presiones muy bajas asociadas al frente polar, que se produce por convergencia de los vientos del oeste y los estes polares, en una zona conocida como bajas presiones subpolares o de ciclones migratorios**

- **En las zonas polares se producen las altas presiones polares, de origen frío, región de nacimiento de los estes polares por la divergencia en superficie.**

![alt text](presionlatitud.png)

![alt text](distribucionviento.png)

### Vientos del Oeste

En los niveles altos de la atmósfera el viento no es del este como lo pronostica el modelo de 3 celdas. En ambos hemisferios el flujo es del oeste. El viento del oeste se produce por los fuertes gradientes de presión que se forman en altura producto de que en las latitudes subtropicales el aire es más calido que en las latitudes altas. Sumado a la fuerza de coriolis, el producen vientos oestes.

Los oestes de altura tiene un movimiento ondulatorio, circundando el planeta formando meandros: entre 3 y 6 alrededor del globo denominadas **ondas de Rossby**

### Corrientes en chorro

El gradiente de presión ecuador-polo aumenta con la altura por lo que la intensidad de los vientos del oeste aumenta.
Inmersas en los oestes se producen angostas franjas de vientos muy intensos, que serpentean por miles de kilómetros de largo como ríos de aire, por esta analogía se les llama 	**corrientes en chorro o jets.**

Como los mayores contrates de temperatura se producen en las zonas frontales en latitudes medias se produce **la corriente en chorro polar** asociada al frente polar, que serpentea con movimiento neto de oeste a este, pero tomando a veces orientación N–S.

En latitudes subtropicales existe otra **corriente en chorro subtropical**, semipermanente, que se produce sólo en invierno, en torno a 25º de latitud y alrededor de 12 km de altura

![alt text](jets.png)
![alt text](jets2.png)

### Conservación de momento angular

A medida que la parcela se desplaza desde el Ecuador hacia 30°, aumenta su velocidad para conservar el momento angular. Al mismo tiempo la fuerza de Coriolis actua para producir vientos del oeste intensos: el jet subtropical. Un jet similar se forma en 60°: el jet polar.

### Continentes 

Los continentes producen perturbaciones el modelo conceptual descripto anteriormente: en lugar de corrientes en chorro del oeste, se obtienen jets superpuestos con un movimiento ondulatorio.

### Ondas de Rosby

Si los vientos son solo del Oeste... ¿Cómo se produce la distribución de calor del ecuador hacia los polos? Eso es gracias a las ondas de Rosby. Donde se forman cuñas y vaguadas que al romperse permiten el transporte de masas de aire frío o cálido.

![alt text](rosby.png)

## CIRCULACIÓN OCEÁNICA

El agua es cerca de 1000 veces más densa que el aire --> Un volumen de agua transporta cerca de 1000 veces más calor que el mismo volumen de aire
Además, la velocidad del aire es miles de veces mas grande que la velocidad de la corriente.

La **salinidad** es la masa de sales disueltas en un kilo de agua de mar. La **densidad** es función de la temperatura y la salinidad.

![alt text](corrientesoceanicas.png)

La **circulación termohalina** es la conducida por variaciones en la densidad del agua debido a diferencias en la temperatura y la salinidad. Domina el flujo en el océano profundo aunque también está acoplada a la circulación conducida por el viento.

## FRENTES Y MASAS DE AIRE

Una **masa de aire** se define como un volumen de aire de gran extensión cuyas propiedades físicas, particularmente temperatura y humedad, son uniformes en el plano horizontal.  La adquisición de las características por parte de las masas de aire es un proceso lento y requiere un *tiempo de permanencia* del aire sobre la superficie. En general, se forman en zonas donde se encuentran sistemas de presión estacionarios caracterizados por una circulación de estancamiento, como el cinturón subtropical, Siberia, Norte de Canadá y ambos polos.

**Clasificación de masas de aire según temperatura**

- *Ártica/antártica*: Son muy estables. Se caracterizan por sus bajas temperaturas y débil contenido de humedad, a consecuencia de lo cual la nubosidad es escasa y el riesgo de precipitaciones muy reducido. 
- *Polar*: Las masas continentales son frías, secas y de estratificación estable porque se forman en las zonas de altas presiones del
interior de Asia Central y Canadá. No existen manantiales en el HS debido al dominio del océano en estas latitudes. Cuando se desplazan al Sur, sobre regiones terrestres más cálidas, aumentan su temperatura y se inestabilizan, dando lugar a la formación de cúmulos pero sin aporte de precipitación. Por el contrario, cuando se desplazan sobre superficies oceánicas el aire inicialmente seco se puede convertir en tropical marítimo formando bancos de niebla o nubes estratiformes (con lloviznas asociadas). Sobre zonas más cálidas pueden desarrollarse sistemas tormentosos.
- *Tropical*: El aire seco procede de las extensas áreas desérticas que crea la subsidencia anticiclónica y es seco, estable y cálido. En verano, el intenso calor que desprende el suelo causa remolinos y tormentas de arena (Sahara, Australia) El aire tropical marítimo es muy húmedo. Propicia la
formación de nieblas de advección, asociadas a nubes estratiformes de poca altitud y lluvias débiles.
- *Ecuatorial*: El aire ecuatorial se caracteriza por tener elevadas temperaturas, alto contenido en humedad y una elevada inestabilidad. Esto posibilita el crecimiento de grandes torres de nubes cúmulos y cumulonimbus, de las que caen lluvias intensas a causa del elevado contenido de humedad absoluta que contiene el aire cálido.

**Clasificación de masas de aire según humedad** 

- *Continental*: tienen un contenido de vapor de agua relativamente bajo, sugiriendo que la condensación será escasa.
- *Marítima*: tienen un alto contenido de vapor de agua indicando que es probable la condensación.

--> Se puede combinar teniendo por ejemplo, una masa de aire *polar continental*. Las que **NO** se pueden formar son *la ártica/antártica marítima* o la *ecuatorial continental*

### Modificaciones en las masas de aire

Cuando una masa de aire es más fría que la superficie sobre la que está pasando luego del símbolo de la masa de aire se agrega la letra k. --> mayor gradiente de T e INESTABILIDAD

Cuando una masa de aire es más cálida que la superficie sobre la que está pasando luego del símbolo de la masa de aire se agrega la letra w --> menor gradiente de T o inversión y ESTABILIDAD 

### Frentes

Siempre, la masa de aire más caliente y liviano es forzada hacia arriba, mientras que el aire más frío y pesado actúa como una cuña sobre la que tiene lugar el ascenso. La formación de los frentes se llama *frontogénesis* y el proceso inverso se llama *frontolisis*.

**Clasificación de frentes**

- 1) *Frente Caliente:* el aire caliente avanza sobre el frío, pero al ser este último más pesado, se pega al suelo y a pesar de retirarse la masa fría, no es desalojada totalmente, de manera que el aire cálido asciende suavemente por la superficie frontal que hace de rampa. En general la nubosidad es estratiforme y las precipitaciones menos intensas que en un frente frío. PENDIENTE --> 1/200

![alt text](frentecaliente.png)

- 2) *Frente Frío*: Como la masa de aire frío es más densa, “ataca" al aire caliente por debajo, como si fuese una cuña, lo levanta, lo desaloja y lo
obliga a trepar cuesta arriba sobre la empinada superficie frontal. El fenómeno es muy violento y en estos ascensos se producen abundantes nubes de desarrollo vertical. PENDIENTE --> 1/100

![alt text](frentefrio.png)

La velocidad promedio de un frente frío es alrededor de 35 km/h en comparación con los 25 km/h de un frente caliente. Por esto y por su mayor pendiente es mucho más violento el frío que el cálido.

- 3) *Frentes ocluidos*: Cuando un frente frío alcanza y sobrepasa un frente caliente, el limite que se crea entre las dos masas de aire se denomina frente ocluido u oclusión. Cuando el aire fresco avanza sobre el más frío será *oclusión caliente* si es al revés será *oclusión fría*.

- 4) *Frente estacionario*: Ocasionalmente, el flujo de aire a ambos lados de un frente no es ni hacia la masa de aire frío ni hacia la de aire cálido, sino que paralelo a la línea del frente, así la posición en superficie del frente no se mueve y se llama frente estacionario.

![alt text](repfrentes.jpg)

## SISTEMAS TÉRMICOS Y SISTEMAS DINÁMICOS

### Baja Fría o dinámica

Es un centro de baja presión en superficie, que por efecto de aire frío presente en su columna vertical central, los espesores se reducen por lo que la baja se intensifica con la altura. Esta baja posee ascenso de aire en su centro con convergencia horizontal en capas bajas y divergencia horizontal en los niveles altos

![alt text](bajafria.png)

### Anticiclón cálido o dinámico

Posee aire caliente en su columna central por lo que los espesores dentro de ella son mayores que el entorno. En consecuencia el anticiclón se intensifica con la altura. También aumenta con la altura la pendiente de las superficies isobáricas, motivo que trae aparejado un incremento de la velocidad del viento. Existe divergencia horizontal en superficie y convergencia horizontal en los niveles altos.


![alt text](anticicloncalido.png)

### Baja cálida o térmica

Tiene un centro de baja presión junto al suelo, el que desaparece ya en los 2 ó 3 km de altura. Más arriba se encuentra una alta que se intensifica con
la altura y abarca casi toda la tropósfera. Esto se debe a la presencia de aire caliente en todos los niveles de la columna vertical central. Los vientos de la baja térmica disminuyen con la altura hasta que se hacen nulos en el nivel donde las superficies isobáricas se hacen horizontales. Más arriba, comienzan a aumentar nuevamente, ya que se incrementan las pendientes de las superficies isobáricas. Entre el suelo y el nivel en que desaparece la baja se distingue ascenso de aire y por encima existe subsidencia.

![alt text](bajacalida.png)

### Anticiclón Frío o térmico

Tiene en su columna central aire frío. La alta de superficie es reemplazada entonces a los 2 ó 3 km por una baja que aumenta su intensidad con la altura. Tiene descenso de aire en las adyacencias al suelo y ascenso en las capas medias y altas de la tropósfera. 

![alt text](anticilonfrio.png)

## TORMENTAS

El aire inestable es aquel que, si se lo desplaza de su nivel inicial tiende a alejarse aún más de dicho nivel. Se requiere alto contenido de humedad para que al ascender el aire se produzca condensación con cierta facilidad. Y por último, debe existir un proceso que dé lugar al ascenso del aire.

**Tormentas de masa de aire:** se producen por efecto de una intensa insolación; también en zonas costeras sobre el continente, por las tardes, cuando el aire frío del mar se desplaza hacia el continente caliente y a la inversa por la noche o madrugada.

**Tormentas frontales:** se producen cuando una masa de aire relativamente fría obliga al ascenso de una masa de aire caliente, húmedo e inestable. En algunas situaciones, el aire caliente y húmedo suele comenzar a ascender por delante del frente frío y puede formarse entonces una línea de tormentas paralela a la zona frontal (Línea de Inestabilidad)

![alt text](vidatormenta.png)

**Las tormentas unicelulares** son el sistema convectivo o tormentoso más simple. Esta formado por una sola estructura cuyo ciclo de vida suele ser de 30-60 minutos y afectar a un área relativamente pequeña y está poco organizado. No suele producir fenómenos adversos en superficie aunque ocasionalmente pueden dar lugar a la formación de granizo, ráfagas y tornados débiles. 

**Tormentas multicelulares** Consisten en un grupo de células que se mueven como una unidad y donde cada célula se encuentra en una etapa diferente del ciclo de vida de la tormenta. A medida que la tormenta se desarrolla, células individuales van ocupando el rol dominante. Corriente arriba se tienden a formar nuevas células, las maduras se ubican en la parte central y las que se disipan están corriente abajo.

### Tormentas severas

- 1) *Linea de Inestabilidad*: Las líneas de inestabilidad ocurren generalmente asociadas a sistemas frontales, particularmente, frentes fríos. Se ubican por delante de los frentes fríos o estacionarios, en el sector caliente (a unos 80 a 150 Km por delante). Usualmente son paralelas al frente y se mueven en su misma dirección, aunque lo hacen una velocidad mayor.

- 2) *Sistemas convectivos de mesoescala*: Los Sistemas Convectivos de Mesoescala, SCM, son estructuras atmosféricas potencialmente generadores de tiempo adverso y severo en superficie que pueden afectar a grandes extensiones de territorio, con ciclos de vida que van más allá de una simple tormenta, pudiendo persistir más de 24 horas ante una buena disponibilidad de humedad en capas bajas.

- 3) *Superceldas*: Generalmente se forman en condiciones de alta inestabilidad, y vientos fuertes a grandes alturas. Además presentan un sistema más organizado de circulación interna que hacen tener una duración mucho mayor que las anteriores. 
