## GASES

**Ley de Boile:** P . V = cte  *(A temperatura constante)*
**Ley de Charles:** V = cte * T  *(A presión constante)*
**Ley de Estado de los gases ideales:** P . V = n . R . T
 
Se puede reemplazar ro = m/v y de esa forma se obtiene: P = ro . R . T

## RADIACION

**Ley del cuadrado inverso** S = So * (ro/r)^2 *(o sea decae como r^2)*

**Ley de Wien** el flujo de radiación que emite un cuerpo negro es máximo para una determinada long de onda que depende inversamente de la temperatura del cuerpo. O sea el máximo del flujo de radiación se produce para una determinada long de onda que depende inversamente de su temperatura.

### lambda_max = beta / T

beta = 2898 micrometro.ºK  

**Ley de Stefan-Boltzmann** El flujo de energía emitido por un cuerpo negro es proporcional a la cuarta potencia de la temperatura absoluta del cuerpo.

### E = sigma * T^4

Donde sigma es una constante.

Generalizandola se puede escribir como E = sigma * epsilon * T^4

donde epsilon es la *emisividad* que es un coeficiente entre 0 y 1.


### **Presión atmosférica = Presión parcial + e del aire seco**

**Humedad absoluta**

### HA = masa de vapor de agua / volumen 

**Humedad específica**

### q = masa de vapor de agua / masa total de aire

**Relación de Mezcla** 

### w = masa de vapor / masa de aire seco

**Humedad relativa**

### HR = ( e / e_s ) * 100
donde e = tensión de vapor , e_s = tensión de vapor de saturación

**ecuación hidrostática:**
###  *dp / dz = - g.rho*