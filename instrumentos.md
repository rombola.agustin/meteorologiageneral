# INSTRUMENTOS DE MEDICIÓN

## RADIACIÓN

**Piroheliómetro:** Mide la energía que proviene directamente del sol, evitando la radiación difusa desde otras direcciones. El instrumento debe ser orientado continuamente hacia el sol. Como sensor se utiliza una placa negra, cuya temperatura, que se mide con un sistema de termocuplas, varía con la radiación solar directa que llega a la placa.

**Piranómetro** Permite evaluar toda la energía solar que llega a una superficie horizontal, incluyendo la radiación directa y la difusa. *Para medir la radiación difusa, se instala un sistema que evita la radiación solar directa sobre el sensor*

![alt text](radiation.png)

**Pirorradiómetro** Permite evaluar toda la energía radiativa que recibe una superficie, incluyendo la radiación solar global y la radiación infrarroja que viene de la atmósfera.

**Heliofanógrafo** El instrumento que registra el período en que el sol alumbra. La heliofanía representa la duración del brillo solar u horas de sol.

*Heliofanía efectiva (d):* Es el período de tiempo (expresado en horas) durante el cual el lugar de observación ha recibido radiación solar directa.

*Heliofanía teórica astronómica (D)* Es el máximo período de tiempo (expresado en horas) durante el cual se podría recibir radiación solar directa, independientemente de las obstrucciones causadas por fenómenos meteorológicos o relieves topográficos, para un lugar y fecha determinados.

*Heliofanía relativa (H):* H = d/D

## OZONO

El ozono se mide en moleculas de ozono por cm3 de aire. La cantidad total de ozono presente en una columna de atmósfera se lo llama **UD: Unidades Dobson** De forma que 1 UD = a una capa de 0,01 mm de espesor a 0ºC y a presión estándar 

**Espectrofotómetro Dobson** Se basa en la técnica de absorción diferencial de la radiación UV por parte de la atmósfera: compara la radiación que llega a una superficie en dos longitudes de onda , una en la que la absorción por parte del O3 es muy importante y otra en la que es despreciable. **La medición es muy precisa pero sólo puede hacerse con el cielo despejado**

**LIDAR** Tiene como corazón a un láser: una pulsación de láser que consta de una luz muy intensa y monocromática y se envía al cielo. Después de un tiempo la radiación dispersada o reemitida vuelve y se mide. De acuerdo con la longitud de onda de la radiación que vuelve se deduce de qué gas está siendo emitida (sólo interesan unas ciertas longitudes correspondientes al ozono) y de acuerdo con la intensidad con que vuelve se deduce en qué concentración se encuentra el gas (ozono). La luz tiene una velocidad concreta; cuanto más tarda después de la pulsación de láser, más altas están las moléculas desde donde la luz vuelve.

![alt text](lidar.png)

**Ozonosonda** Consisten de un paquete de instrumentos que son colgados de un globo sonda meteorológico, que se infla con un gas menos denso que el aire (H o He) por lo que asciende a velocidad constante hasta la estratósfera (aprox. 35 km / 5 hPa). El paquete de instrumentos registra T, P, HR y concentración de ozono en el aire y envía los datos instantáneamente a la estación receptora en tierra.

**Mediciones satelitales** En 1979 se iniciaron mediciones en forma continua a través del instrumento *TOMS* (sigla en inglés a partir de Total Ozone Mapping Spectrometer) montado en el satélite Nimbus-7. Los resultados muestran que, en promedio, la cantidad de ozono es mayor durante la primavera y en latitudes medias y altas y es menor en la región ecuatorial.

## TEMPERATURA

**Termómetro:** registra la temperatura. Se basa en el principio de que un líquido (en gral. mercurio o alcohol) cambia de volumen en función de la temperatura

**Termómetro de máxima:** registra la temperatura más alta del día. Es un termómetro de mercurio que tiene un estrechamiento del capilar cerca del bulbo o depósito. Cuando la temperatura sube, la dilatación de todo el mercurio del bulbo vence la resistencia opuesta por el estrechamiento, mientras que cuando la temperatura baja y la masa de mercurio se contrae, la columna se rompe por el estrechamiento y su extremo libre queda marcando la temperatura máxima. 

**Termómetro de mínima:** registra la temperatura más baja del día. Está compuesto de alcohol y llevan un índice coloreado de vidrio o marfil sumergido en el líquido. El bulbo tiene en general forma de horquilla (para aumentar la superficie de contacto del elemento sensible). Cuando la
temperatura baja, el líquido arrastra el índice porque no puede atravesar el menisco y se ve forzado a seguir su recorrido de retroceso. Cuando la temperatura sube, el líquido pasa fácilmente entre la pared del tubo y el índice y éste queda marcando la temperatura más baja por el extremo más alejado del bulbo. La escala está dividida cada 0,5ºC y su amplitud va desde -44,5 a 40,5ºC. 

**Termógrafo** (grafica la temperatura a través del tiempo). El sensor de este instrumento está constituido por un elemento bimetálico circular. Es decir dos metales de diferente coeficiente de dilatación (ínvar y bronce o ínvar y acero). Cuando varía la temperatura se produce un cambio en el radio del
elemento medidor que se transmite a un sistema de palancas que accionan un brazo inscriptor. La banda de registro va colocada sobre un tambor cilíndrico que contiene un mecanismo de relojería. Este gira una vuelta en 24 horas o en una semana según se seleccione. La escala está dividida de a 1ºC. La amplitud es de -35 a 45ºC y la precisión es de +-0,5ºC.

## HUMEDAD 

**Tanque de evaporación:** es el instrumento que se utiliza más a menudo para estimar la evaporación que se produce desde una superficie de agua. La versión más difundida tiene un diámetro de 120 cm y una profundidad de 25.4 cm. 

**Higrómetro:** mide la humedad relativa. Este instrumento tiene un haz de cabello cuyo extremo superior está fijado al armazón y el inferior sujeta un peso. El peso está conectado por palancas amplificadoras a un sistema de transmisión que termina en un señalador que, moviéndose sobre una escala, indica la humedad relativa. El higrómetro se puede transformar en un aparato registrador y entonces se llama **higrógrafo**.

**Psicrómetro:** mide la temperatura de bulbo seco y la temperatura de bulbo húmedo (mide la humedad en forma indirecta) La temperatura de bulbo seco se corresponde con la temperatura ambiental tal como se mide normalmente. El termómetro de bulbo húmedo tiene su bulbo envuelto en una muselina que se mantiene siempre humedecida. 

*Conociendo la temperatura del bulbo seco y la del bulbo húmedo podemos conocer las condiciones ambientales de humedad a través de una ecuación (tabla)*

## NUBES

La cantidad de nubes que cubre el cielo se mide en octavos. 

- 0: Despejado
- 1 y 2: ligeramente nublado
- 3 y 4: algo nublado
- 5, 6 y 7: parcialmente nublado 
- 8: totalmente cubierto

## PRECIPITACIÓN

**Pluviómetro:** milímetro de agua caída 1mm corresponde a un litro de agua por m2 de superficie
**Pluviógrafo:** 
**Medición por radar o satélite**

## PRESIÓN

**Barómetro de Mercurio (Hg):** Lo que se mide es la altura de una columna de mercurio cuyo peso es compensado por la presión de la atmósfera.Se coloca en el interior de la estación meteorológica, ya que no puede estar expuesto al sol ni a corrientes de aire. Una vez leído el dato de presión se deben hacer algunas correcciones:
- Por temperatura, ya que la altura del mercurio varía con la temperatura
- Por gravedad (reducir a 45º de latitud y 0 metros)

**Barómetro aneroide:** Está constituido por una cámara en cuyo interior se ha hecho vacío. Una de las paredes de la cámara actúa como un diafragma que se deforma en respuesta a los cambios de presión exterior. Es frecuente que se coloquen varias cámaras en series para amplificar la señal.

## VIENTO

**Anemómetro:** mide la velocidad del viento. La dirección del viento se mide con una veleta, algunas veces incorporada con el anemómetro.

Existe la Escala de Beaufort es una medida empírica para la intensidad del viento basada principalmente en el estado del mar y de sus olas

## VARIOS

**Radiosonda** : Una radiosonda es un dispositivo empleado en globos meteorológicos para medir varios parámetros atmosféricos y transmitirlos a un aparato receptor fijo. La frecuencia de radio de 403 MHz está reservada para uso con las radiosondas. 


